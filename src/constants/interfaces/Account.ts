export interface IAccount {
  totalCredits: string;
  totalDebits: string;
  endOfDayBalances: DayBalance[];
}

export interface DayBalance {
  date: string;
  balance: string;
}
