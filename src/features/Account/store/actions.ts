import { IAccount } from "../../../constants/interfaces/Account";
export enum Actions {
  POST_ACCOUNT_START = "POST_ACCOUNT_START",
  POST_ACCOUNT_SUCCESS = "POST_ACCOUNT_SUCCESS",
  POST_ACCOUNT_FAILURE = "POST_ACCOUNT_FAILURE",
}

export function postAccountStart() {
  return {
    type: Actions.POST_ACCOUNT_START as Actions.POST_ACCOUNT_START,
  };
}

export function postAccountSuccess(payload: IAccount[]) {
  return {
    type: Actions.POST_ACCOUNT_SUCCESS as Actions.POST_ACCOUNT_SUCCESS,
    payload,
  };
}

export function postAccountFailure(error: string) {
  return {
    type: Actions.POST_ACCOUNT_FAILURE as Actions.POST_ACCOUNT_FAILURE,
    payload: error,
  };
}

export type AccountActions =
  | ReturnType<typeof postAccountStart>
  | ReturnType<typeof postAccountSuccess>
  | ReturnType<typeof postAccountFailure>;
