import { Reducer } from "redux";
import { IAccount } from "../../../constants/interfaces/Account";
import { Actions, AccountActions } from "./actions";

export interface AccountState {
  data: IAccount[];
  loading: boolean;
  error: string;
}

const initialState: AccountState = {
  data: [] as IAccount[],
  loading: false,
  error: "",
};

const accountReducer: Reducer<AccountState, AccountActions> = (
  state = initialState,
  action: AccountActions
): AccountState => {
  switch (action.type) {
    case Actions.POST_ACCOUNT_START:
      return {
        ...state,
        loading: true,
      };
    case Actions.POST_ACCOUNT_SUCCESS:
      return {
        ...state,
        loading: false,
        data: action.payload,
      };
    case Actions.POST_ACCOUNT_FAILURE:
      return {
        ...state,
        loading: true,
        error: action.payload,
      };
    default:
      return state;
  }
};

export default accountReducer;
