import { usePostAccountApi } from "../api";
import { useCallback } from "react";
import { useDispatch } from "react-redux";
import {
  postAccountStart,
  postAccountSuccess,
  postAccountFailure,
} from "./actions";
import { handleError } from "../../../helpers/handleError";
import { ApiResponse } from "../../../constants/interfaces/ApiResponse";
import { IAccount } from "../../../constants/interfaces/Account";
import { toast } from "react-toastify";

export const usePostAccountService = () => {
  const postAccountApi = usePostAccountApi();
  const dispatch = useDispatch();
  
  return useCallback(
    (data: any): Promise<void> => {
      dispatch(postAccountStart());
      return postAccountApi(data)
        .then((response: ApiResponse<IAccount[]>) => {
          dispatch(postAccountSuccess(<IAccount[]>response.data));
          toast.success('Loaded Accounts Successfully');
        })
        .catch((error: any) => {
          let errorMessage = handleError(error);
          dispatch(postAccountFailure(errorMessage));
          toast.error(errorMessage);
        });
    },
    [dispatch, postAccountApi]
  );
};
