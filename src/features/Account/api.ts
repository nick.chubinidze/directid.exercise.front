import { useCallback } from "react";
import { Http } from "../../services/http";

export function usePostAccountApi() {
  return useCallback((data: any): Promise<any> => {
    return Http.post(`account/getEndOfDayBalances`, data);
  }, []);
}
