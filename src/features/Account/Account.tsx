import { FC } from "react";
import { useSelector } from "react-redux";
import { RootState } from "../../store/store";

type Props = {};
const Account: FC<Props> = () => {
  const { loading, data } = useSelector((state: RootState) => state.account);

  return (
    <>
      {loading ? (
        <div className="vw-100 vh-100 d-flex align-items-center justify-content-center">
          <div className="spinner-border text-info" role="status"></div>
        </div>
      ) : (
        data.map((account, index) => (
          <div className="d-flex py-2 align-items-center flex-column" key={index}>
            <div className="text-info">Account-{index} Details</div>
            <div className="pt-3">
              <div className="text-dark">Total Credits: {account.totalCredits}</div>
              <div className="text-dark mb-10">Total Debits: {account.totalDebits}</div>              
              {account.endOfDayBalances &&
                account.endOfDayBalances.map((item, index) => (
                  <div className="pb-2 pt-3" key={index}>
                    <span className="d-block text-dark">Date: {new Date(item.date).toDateString()}</span>
                    <span className="text-dark">Balance: {item.balance}</span>
                  </div>
                ))}
            </div>
          </div>
        ))
      )}
    </>
  );
};

export default Account;
