import { combineReducers } from "redux";
import accountReducer from "../features/Account/store/reducers";
const rootReducer = combineReducers({
  account: accountReducer,
});

export default rootReducer;
