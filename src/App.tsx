import { useEffect } from "react";
import { usePostAccountService } from "./features/Account/store/services";
import Account from "./features/Account/Account";
import AccountInput from "./constants/dummy/AccountInput";
import { ToastContainer } from "react-toastify";

function App() {
  const postAccout = usePostAccountService();
  useEffect(() => {
    postAccout(AccountInput);
  }, [postAccout]);
  return (
    <div className="App">
      <Account />
      <ToastContainer autoClose={5000} />
    </div>
  );
}

export default App;
