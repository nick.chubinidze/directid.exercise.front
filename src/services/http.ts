const axios = require("axios");

export class Http {
  static post(path: string, data?: any) {
    return axios.post(`${process.env.REACT_APP_ENDPOINT}${path}`, data);
  }
  static get(path: string, data?: any): Promise<any> {
    return axios.get(`${process.env.REACT_APP_ENDPOINT}${path}`, data);
  }
  static put(path: string, data: any) {
    return axios.put(`${process.env.REACT_APP_ENDPOINT}${path}`, data);
  }
  static delete(path: string, data?: any) {
    return axios.delete(`${process.env.REACT_APP_ENDPOINT}${path}`, data);
  }
}
